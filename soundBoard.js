/*
"Events are actions or occurrences that happen in the system you are programming, which the system tells you about so you can respond to them in some way if desired.

For example, if the user clicks a button on a webpage, you might want to respond to that action by displaying an information box.

EventTarget is a DOM interface implemented by objects that can receive events and may have listeners for them.

Element, Document, and Window are the most common event targets, but other objects can be event targets too, for example XMLHttpRequest, AudioNode, AudioContext, and others.

The EventListener interface represents an object that can handle an event dispatched by an EventTarget object.

" - MDN web docs
*/

function playSound(e) {
  const audio = document.querySelector(`audio[data-key="${e.keyCode}"]`);
  const key = document.querySelector(`.pad[data-key="${e.keyCode}"]`);
  if (!audio) return;
  audio.currentTime = 0; //start audio over again
  audio.play(); //play audio
  console.log(e.keyCode);
  key.classList.add('playing');
};

function removeTransition(e) {
  if (e.propertyName !== 'transform') return;
  this.classList.remove('playing');
}

const keys = document.querySelectorAll(".pad");
keys.forEach(key => key.addEventListener('transitionend', removeTransition));

window.addEventListener('keydown', playSound);
